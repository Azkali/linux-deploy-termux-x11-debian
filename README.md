# Installation

Run the first time setup script in termux:
```
pkg in -y root-repo x11-repo
pkg in -y wget tsu termux-x11-nightly
wget https://gitlab.com/azkali/linux-deploy-termux-x11-debian/-/raw/master/scripts/setup.sh
chmod +x ./setup.sh
./setup.sh
```

The script will install needed dependencies for termux and will setup a chroot using the latest artifact from the pipeline of this repo.

Once you're past the setup you can run the chroot as follow:
```
linux-deploy
```

From there, you can launch LXQT with `launchx` command which has everything setup and has working virgl passthrough

# Credits

Repo forked from: https://github.com/Pipetto-crypto/Chroot-Docs/
