#!/bin/bash

BASE_URL="https://gitlab.com/azkali/linux-deploy-termux-x11-debian/-/raw/master"
SCRIPTS_DIR="${BASE_URL}/scripts/"
LATEST_ROOTFS="https://gitlab.com/azkali/linux-deploy-termux-x11-debian/-/jobs/artifacts/master/raw/ubuntu.tar.bz2?job=build"
TERMUX_HOME="/data/data/com.termux/files/home/"
CHROOT_DIR="${TERMUX_HOME}/ubuntu/"

echo -e "Installing needed termux dependencies\n"
pkg update && pkg upgrade -y
pkg install -y root-repo x11-repo tur-repo
pkg update
pkg install -y mount-utils pulseaudio wget termux-x11-nightly mesa-zink virglrenderer-mesa-zink vulkan-loader-android

echo -e "Retrieving latest script and latest ROOTFS\n"
wget -O ${PATH}/linux-deploy ${SCRIPTS_DIR}/linux-deploy
chmod +x ${PATH}/linux-deploy
sudo wget -O ${TERMUX_HOME}/ubuntu.tar.bz2 ${LATEST_ROOTFS}

sudo rm -rf ${CHROOT_DIR}
sudo mkdir -p ${CHROOT_DIR}
sudo tar xf ${TERMUX_HOME}/ubuntu.tar.bz2 -C ${CHROOT_DIR}
sudo rm ${TERMUX_HOME}/ubuntu.tar.bz2

echo -e "Reloading termux\n"
sed -i 's/# allow-external-apps/allow-external-apps/g' ~/.termux/termux.properties
termux-reload-settings

echo -e "Done !\nPlease install termux-x11 using the latest artifacts from:"
echo -e "https://github.com/termux/termux-x11/actions/workflows/debug_build.yml"
