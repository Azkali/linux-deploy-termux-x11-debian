#!/bin/bash
set -xe
rm -rf /noble; mkdir -p /noble

CWD=$(dirname -- "${BASH_SOURCE[0]}")

debootstrap \
	--components="main,restricted,universe,multiverse" \
	--variant=buildd \
	--arch=arm64 \
	--include="gpg,gpg-agent,ca-certificates" \
	noble /noble

cp ${CWD}/../patches/*.patch /noble

arch-chroot /noble /bin/bash << EOF
# Setup android groups
groupadd -g 3001 aid_bt
groupadd -g 3002 aid_bt_net
groupadd -g 3003 aid_inet
groupadd -g 3004 aid_net_raw
groupadd -g 3005 aid_admin

useradd -G 3001,3002,3003,3004,3005,wheel,video,audio -m -s /bin/bash user
echo user:user | chpasswd
usermod -a -G 3001,3002,3003,3004,3005 root
usermod -a -G 3003 _apt
usermod -g 3003 _apt

echo "deb-src http://ports.ubuntu.com/ubuntu-ports noble main restricted universe multiverse" >> /etc/apt/sources.list

dpkg --add-architecture armhf

apt update && apt install -y \
	passwd \
	sudo \
	wget \
	nano \
	vim \
	kde-plasma-desktop \
	software-properties-common \
	dbus-x11 \
	binfmt-support \
	git \
	meson \
	ninja-build \
	libpolly-17-dev

# Compile mesa from robclark
apt -y build-dep mesa
git clone -b mesa/no-drm-fixes https://gitlab.freedesktop.org/robclark/mesa
cd mesa/

git apply /*.patch

mkdir build-zink
meson build-zink -D platforms=x11 -Dgallium-drivers=swrast,zink,virgl -D vulkan-drivers= -D dri3=enabled -D egl=enabled -D gles2=enabled -D glvnd=true -D glx=dri -D libunwind=disabled -D shared-glapi=enabled -Dshared-llvm=disabled -D microsoft-clc=disabled -D valgrind=disabled -D gles1=disabled -Dbuildtype=release -Dprefix=/usr
ninja -j$(nproc) -C build-zink
ninja -C build-zink install

mkdir build-turnip
meson build-turnip -D platforms=x11 -Dgallium-drivers= -D vulkan-drivers=freedreno -D dri3=enabled -D gles2=enabled -D glvnd=true -D glx=dri -D libunwind=disabled -D shared-glapi=enabled -D microsoft-clc=disabled -D valgrind=disabled -D gles1=disabled -D freedreno-kmds=kgsl -Dbuildtype=release -Dprefix=/usr
ninja -j$(nproc) -C build-turnip
ninja -C build-turnip install

cd ../
rm -rf mesa /*.patch

# Box86
wget https://itai-nelken.github.io/weekly-box86-debs/debian/box86.list -O /etc/apt/sources.list.d/box86.list
wget -qO- https://itai-nelken.github.io/weekly-box86-debs/debian/KEY.gpg | apt-key add -

# Box64
wget https://ryanfortner.github.io/box64-debs/box64.list -O /etc/apt/sources.list.d/box64.list
wget -qO- https://ryanfortner.github.io/box64-debs/KEY.gpg | apt-key add -

# Box64 Bash
wget https://github.com/ptitSeb/box64/raw/main/tests/bash -O /usr/local/bin/box_bash
chmod +x /usr/local/bin/box_bash

# FEX
add-apt-repository -y ppa:fex-emu/fex
apt update && apt install -y fex-emu-armv8.4 fex-emu-binfmt* fex-utils box86 box64-android

# Setup rootfs workarounds
echo "nameserver 8.8.8.8" > /etc/resolv.conf
echo "127.0.0.1 localhost" > /etc/hosts

echo 'update-binfmts --enable' >> /etc/bash.bashrc
EOF

cp "$(dirname ${BASH_SOURCE[0]})"/../launchx /noble/usr/local/bin/
tar \
	--acls \
	--xattrs \
	--numeric-owner \
	--exclude='mnt' \
	--exclude='media' \
	--exclude='srv' \
	--exclude='boot' \
	-jScf ubuntu.tar.bz2 \
	-C /noble/ .
